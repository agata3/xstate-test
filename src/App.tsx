import './App.css'
//peasy https://xstate.js.org/viz/?gist=5ebc312524241c991aa37a1bb3fec8b6&fbclid=IwAR2ODdiM9HPH3lWYQZne_0hCa0mINfuGsXcWlcUAEHUj1_yAIvvT9r5knsI

import { Box, Typography, Button } from '@mui/material'
import { createMachine } from 'xstate'
import { useMachine } from '@xstate/react'
import { CssBaseline } from '@material-ui/core'
import { Adress } from './pages/Adress'
import { Name } from './pages/Name'
import { Payment } from './pages/Payment'
import { Submit } from './pages/Submit'
import { useState } from 'react'

const booking = createMachine({
    id: 'transaction',
    initial: 'new',
    states: {
        new: {
            on: {
                NEXT: 'name',
            },
        },
        name: {
            on: {
                NEXT: 'adress',
            },
        },
        adress: {
            on: {
                NEXT: 'payment',
            },
        },
        payment: {
            // initial: "paymentOptions",
            // states: {
            //   paymentOptions: {
            on: {
                PAYPAL: '#transaction.submit',
                VISA: '#transaction.submit',
                MASTERCARD: '#transaction.submit',
            },
            //   },
            // },
        },
        submit: {
            type: 'final',
        },
    },
})

function App() {
    const [state, send] = useMachine(booking)
    const [paymentOption, setPaymentOption] = useState<
        'PAYPAL' | 'VISA' | 'MASTERCARD' | null
    >(null)
    return (
        <Box m={5} width='100vw' height='100vh'>
            <CssBaseline />

            <Box my={1}>
                {state.value === 'new' && <Typography>Welcome</Typography>}
                {/* <Typography variant="h5">{state.value.toString()}</Typography> */}
            </Box>
            {state.value === 'name' && <Name />}
            {state.value === 'adress' && <Adress />}
            {state.value === 'payment' && (
                <Payment
                    paymentOption={paymentOption}
                    setPaymentOption={setPaymentOption}
                />
            )}
            {state.value === 'submit' && <Submit />}
            {paymentOption === 'PAYPAL' && (
                <Button onClick={() => send('PAYPAL')} variant='contained'>
                    CONTINUE
                </Button>
            )}
            {paymentOption === 'MASTERCARD' && (
                <Button onClick={() => send('MASTERCARD')} variant='contained'>
                    CONTINUE
                </Button>
            )}
            {paymentOption === 'VISA' && (
                <Button onClick={() => send('VISA')} variant='contained'>
                    CONTINUE
                </Button>
            )}
            {state.value === 'new' ||
            state.value === 'adress' ||
            state.value === 'name' ? (
                <Button onClick={() => send('NEXT')} variant='contained'>
                    CONTINUE
                </Button>
            ) : null}
        </Box>
    )
}

export default App

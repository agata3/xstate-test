import { Box, Typography } from "@material-ui/core";

export interface WelcomeProps {}

export const Welcome: React.FC<WelcomeProps> = () => {
  return (
    <Box>
      <Typography>Welcome </Typography>
    </Box>
  );
};

import { Box, Typography, Button } from "@material-ui/core";

export interface PaymentProps {
  paymentOption: "PAYPAL" | "VISA" | "MASTERCARD" | null;
  setPaymentOption: React.Dispatch<
    React.SetStateAction<"PAYPAL" | "VISA" | "MASTERCARD" | null>
  >;
}

export const Payment: React.FC<PaymentProps> = ({
  paymentOption,
  setPaymentOption,
}) => {
  return (
    <Box>
      <Typography>Payment options</Typography>
      <Box marginBottom={1}></Box>
      <Button
        onClick={() => setPaymentOption("PAYPAL")}
        variant={paymentOption === "PAYPAL" ? "contained" : "outlined"}
      >
        PAYPAL
      </Button>
      <Button
        onClick={() => setPaymentOption("MASTERCARD")}
        variant={paymentOption === "MASTERCARD" ? "contained" : "outlined"}
      >
        MASTERCARD
      </Button>
      <Button
        onClick={() => setPaymentOption("VISA")}
        variant={paymentOption === "VISA" ? "contained" : "outlined"}
      >
        VISA
      </Button>
    </Box>
  );
};

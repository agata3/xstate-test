import { Box, Typography } from "@material-ui/core";

export interface NameProps {}

export const Name: React.FC<NameProps> = () => {
  return (
    <Box>
      <Typography>Your name</Typography>
    </Box>
  );
};

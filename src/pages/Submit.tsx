import { Box, Typography } from "@material-ui/core";

export interface SubmitProps {}

export const Submit: React.FC<SubmitProps> = () => {
  return (
    <Box>
      <Typography>Submit</Typography>
    </Box>
  );
};
